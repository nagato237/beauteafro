-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 28 mars 2021 à 11:10
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `beauteafro`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonnement`
--

DROP TABLE IF EXISTS `abonnement`;
CREATE TABLE IF NOT EXISTS `abonnement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `duree` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `abonnement`
--

INSERT INTO `abonnement` (`id`, `intitule`, `price`, `duree`) VALUES
(2, 'mois', 100, 30),
(3, 'semaine', 10, 7),
(4, 'année', 300, 365);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intitule` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `intitule`, `description`) VALUES
(1, 'humour', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet venenatis erat. Curabitur sodales odio eget dui ultrices, sit amet vulputate urna fermentum. Phasellus eu ipsum et mi consectetur pulvinar sit amet vel nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consequat a tellus at iaculis. Etiam non erat ipsum. Morbi et commodo mi, eu fringilla urna. Vestibulum ut nisi consequat, malesuada ipsum eu, sollicitudin magna. Donec eget ipsum nec odio iaculis finibus in sodales arcu. Fusce at lectus vestibulum, tempor felis quis, placerat dui. Aliquam hendrerit dui eget felis viverra, eu tristique lectus aliquam. Nunc dolor metus, viverra sed lacus a, ornare pretium neque. In hac habitasse platea dictumst. '),
(2, 'serieux', 'Nulla felis ante, ultrices eget risus eget, porta mollis magna. Cras tincidunt tellus sit amet odio ultrices, a sagittis magna mattis. Sed mollis ultricies enim quis dictum. Nullam auctor ante vel tincidunt hendrerit. Ut tincidunt libero magna, eget molestie orci pellentesque eu. Sed eleifend, quam facilisis ullamcorper aliquet, ipsum orci laoreet lorem, fermentum vestibulum turpis turpis eu orci. Curabitur mollis sem at lacus volutpat, at condimentum turpis suscipit. Donec nec euismod ligula, vitae rutrum purus. '),
(3, 'allo', 'allo'),
(4, 'encore', 'toujours'),
(5, 'testencore', 'test2again'),
(6, 'test4', ' test444444'),
(7, 'test4', ' test444444'),
(8, 'test categorie', 'oui oui '),
(9, 'toutou', 'toutou desc'),
(10, 'toutou', 'toutou desc');

-- --------------------------------------------------------

--
-- Structure de la table `chapitre`
--

DROP TABLE IF EXISTS `chapitre`;
CREATE TABLE IF NOT EXISTS `chapitre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pdf_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `create_at` datetime NOT NULL,
  `son_livre_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8C62B025DB13B753` (`son_livre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `chapitre`
--

INSERT INTO `chapitre` (`id`, `pdf_path`, `titre`, `description`, `create_at`, `son_livre_id`) VALUES
(1, '....', '....', '1', '2020-08-15 10:24:50', 1),
(2, '...', '...', '2', '2020-08-17 10:25:07', 1),
(3, '1', '2', '3', '2020-08-20 10:25:31', 2),
(4, '2', '3', '4', '2020-08-22 10:25:55', 1),
(9, 'pdf/test pdf_90.pdf', 'test pdf', 'allo', '2021-01-16 11:54:13', 9),
(10, 'pdf/test2.pdf', 'livre 2', 'desc toto', '2021-02-27 12:03:01', 10);

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE IF NOT EXISTS `commentaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chapitre_id` int(11) NOT NULL,
  `contenu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_67F068BC1FBEEF7B` (`chapitre_id`),
  KEY `IDX_67F068BCA76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`id`, `chapitre_id`, `contenu`, `create_at`, `user_id`) VALUES
(1, 9, 'premier commentaire ', '2021-02-27 12:06:44', 17),
(2, 10, 'second commentaire', '2021-02-26 12:07:15', 18),
(3, 9, 'troisieme commentaire ', '2021-02-27 12:06:44', 17);

-- --------------------------------------------------------

--
-- Structure de la table `facture`
--

DROP TABLE IF EXISTS `facture`;
CREATE TABLE IF NOT EXISTS `facture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `abonnement_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `actif` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_FE866410F1D74413` (`abonnement_id`),
  KEY `IDX_FE866410A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `facture`
--

INSERT INTO `facture` (`id`, `user_id`, `abonnement_id`, `created_at`, `actif`) VALUES
(1, 17, 3, '2021-02-27 12:15:41', 1);

-- --------------------------------------------------------

--
-- Structure de la table `livre`
--

DROP TABLE IF EXISTS `livre`;
CREATE TABLE IF NOT EXISTS `livre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path_miniature` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_at` datetime NOT NULL,
  `chapitre_id` int(11) DEFAULT NULL,
  `categorie_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_AC634F991FBEEF7B` (`chapitre_id`),
  KEY `IDX_AC634F99BCF5E72D` (`categorie_id`),
  KEY `IDX_AC634F99A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `livre`
--

INSERT INTO `livre` (`id`, `description`, `titre`, `path_miniature`, `create_at`, `chapitre_id`, `categorie_id`, `user_id`) VALUES
(1, 'description livre 1', 'titre livre 1', 'miniatures/image1.jpg', '2020-07-23 13:56:11', NULL, 1, NULL),
(2, 'description livre 2', '22222', 'miniatures/image2.jpg', '2020-07-22 19:20:22', NULL, 1, NULL),
(3, 'description livre 3', '3333', 'miniatures/image3.jpg', '2020-07-27 19:21:03', NULL, 1, NULL),
(9, 'allo', 'test', 'miniatures/test_61.jpg', '2021-02-20 11:35:55', NULL, 7, NULL),
(10, 'alloee', 'testtt', 'miniatures/testtt_34.jpg', '2021-02-20 11:41:04', NULL, 7, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20191123140639', '2019-11-23 14:07:17'),
('20191123141249', '2019-11-23 14:12:56'),
('20191123142119', '2019-11-23 14:21:31'),
('20191201110631', '2019-12-01 11:07:17'),
('20191201112232', '2019-12-01 11:22:44'),
('20191201113111', '2019-12-01 11:31:21'),
('20191205135010', '2019-12-05 13:50:26'),
('20191205140811', '2019-12-05 14:08:31'),
('20200704071842', '2020-07-04 07:19:20'),
('20200704072511', '2020-07-04 07:25:20');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `localisation` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `localisation`, `roles`, `password`, `is_active`, `email`) VALUES
(17, 'ttetyuo', 'American Samoa', '[]', '$2y$13$HggXRcPU.VzbAwlO1kX6VuIGJTGv3mvavBNq9bMWaNAAaus4FqKFm', 1, 'e@f.fr'),
(18, 'booh', 'Bahamas', '[]', '$2y$13$qjYvLsO4cqJiPI0hD14T3eH.H/b6DjYyKR/1dFWqRofhLC36MTtGu', 1, 'ngfdie@gmail.com'),
(19, 'test', 'Cameroon', '[]', '$2y$13$YTc.fk8W4KrcWH.WUx8cM.iYApEgE45l8wYBo/l2Ms2B5SS13RPRi', 1, 'test@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `video`
--

DROP TABLE IF EXISTS `video`;
CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `miniature` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `video`
--

INSERT INTO `video` (`id`, `path`, `titre`, `description`, `create_at`, `miniature`) VALUES
(3, 'https://youtu.be/5QvLTbKA1UM', '32GB', 'video Danse', '2021-02-27 12:18:45', 'miniatures/test.jpg'),
(4, 'https://youtu.be/bPHd-zhpPdg', '32Gb moi ', 'description ', '2021-02-27 12:19:23', 'miniatures/test2.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `video_categorie`
--

DROP TABLE IF EXISTS `video_categorie`;
CREATE TABLE IF NOT EXISTS `video_categorie` (
  `video_id` int(11) NOT NULL,
  `categorie_id` int(11) NOT NULL,
  PRIMARY KEY (`video_id`,`categorie_id`),
  KEY `IDX_8B02ABA129C1004E` (`video_id`),
  KEY `IDX_8B02ABA1BCF5E72D` (`categorie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `video_categorie`
--

INSERT INTO `video_categorie` (`video_id`, `categorie_id`) VALUES
(3, 8),
(4, 9);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `chapitre`
--
ALTER TABLE `chapitre`
  ADD CONSTRAINT `FK_8C62B025DB13B753` FOREIGN KEY (`son_livre_id`) REFERENCES `livre` (`id`);

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `FK_67F068BC1FBEEF7B` FOREIGN KEY (`chapitre_id`) REFERENCES `chapitre` (`id`),
  ADD CONSTRAINT `FK_67F068BCA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `facture`
--
ALTER TABLE `facture`
  ADD CONSTRAINT `FK_FE866410A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_FE866410F1D74413` FOREIGN KEY (`abonnement_id`) REFERENCES `abonnement` (`id`);

--
-- Contraintes pour la table `livre`
--
ALTER TABLE `livre`
  ADD CONSTRAINT `FK_AC634F991FBEEF7B` FOREIGN KEY (`chapitre_id`) REFERENCES `chapitre` (`id`),
  ADD CONSTRAINT `FK_AC634F99A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_AC634F99BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`);

--
-- Contraintes pour la table `video_categorie`
--
ALTER TABLE `video_categorie`
  ADD CONSTRAINT `FK_8B02ABA129C1004E` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_8B02ABA1BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
