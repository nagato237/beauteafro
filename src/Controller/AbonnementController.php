<?php
/**
 * Created by PhpStorm.
 * User: Franklin M
 * Date: 7/22/2020
 * Time: 4:12 PM
 */

namespace App\Controller;
use App\Entity\Abonnement;
use App\Entity\Facture;
use DateInterval;
use FOS\RestBundle\Controller\Annotations as Rest;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 
 * Class AbonnementController
 * @package App\Controller
 */
class AbonnementController extends AbstractController
{

    /**
     * @Rest\Get(path="/abonnement/{id}", name="show_abonnement")
     * @Rest\View()
     */
    public function getAbonnement(Abonnement $abonnement){

        return $abonnement;

    }

    /**
     * @Rest\Post(path="/saveaboonement/", name="save_abonnement")
     * @Rest\View()
     * @ParamConverter("abonnement", converter="fos_rest.request_body")
     */
    public  function saveAbonnement(Abonnement $abonnement){
        $em = $this->getDoctrine()->getManager();
        $em->persist($abonnement);
        $em->flush();

        return $abonnement;
    }

    /**
     * @Rest\Get(path="/abonnements/", name="show_abonnements")
     * @Rest\View()
     */
    public function getAbonnements(Request $request){
        $pagination = new  Paginator();
        $abonnement = $this->getDoctrine()->getRepository(Abonnement::class)->findAll();

        $abonnement = $pagination->paginate($abonnement, $request->query->getInt('page', 1), 8);
        return $abonnement;

    }


/**
     * @Rest\Post(path="/saveaboonement/", name="save_abonnement")
     * @Rest\View()
     */
    public  function PrendreAbonnement(Request $request){
        $em = $this->getDoctrine()->getManager();
        /** On recupere l'abonnement choisi par lutilisateur */
        $abonnement_id = $request->files->get('abonnement');
        $abonnement = $em->getRepository(Abonnement::class)->find($abonnement_id);

/** on effectue le paiement */

/** si ok alors on enregistre une facture avec le nombre mois  */
        $facture = new Facture;
        $date = new \DateTime('now');
        $facture->setCreatedAt($date);
        $facture->setUser($this->getUser());
        $facture->setFinishedAt($date->add(new DateInterval('P'.$abonnement->getDuree().'D')));
        $facture->setActif(1);
        $em->persist($facture);
        $em->flush();

        return $abonnement;
    }

}