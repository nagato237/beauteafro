<?php
/**
 * Created by PhpStorm.
 * User: Franklin M
 * Date: 7/4/2020
 * Time: 10:02 AM
 */

namespace App\Controller;

use App\Entity\Categorie;
use FOS\RestBundle\Controller\Annotations as Rest;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**

 * Class CategorieController
 * @package App\Controller
 */
class CategorieController extends  AbstractController
{
    /**
     * @Rest\Get(path="/categorie/{id}", name="show_categrorie", requirements={"id" = "\d+"})
     * @Rest\View()
     */
    public  function showCategrorie(Categorie $categorie){

    return $categorie;
    }

    /**
     * @Rest\Post(path="/categorie/", name="categorie_add" )
     * @Rest\View()
     * @ParamConverter("categorie", converter="fos_rest.request_body")
     */
    public function saveCategorie(Categorie $categorie){

        $em = $this->getDoctrine()->getManager();
        $em->persist($categorie);
        $em->flush();

        return $categorie;
    }


    /**
     * @Rest\Get(path="/categories/", name="show_categrorie")
     * @Rest\View()
     */
    public  function showCategrories( Request $request){
        $pagination = new  Paginator();
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $categories = $pagination->paginate($categories, $request->query->getInt('page', 1), 8);
        return $categories;
    }

}