<?php
/**
 * Created by PhpStorm.
 * User: Franklin M
 * Date: 7/22/2020
 * Time: 4:12 PM
 */

namespace App\Controller;
use App\Entity\Chapitre;
use App\Entity\Livre;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route(path="/api")
 * Class ChapitreController
 * @package App\Controller
 */
class ChapitreController extends AbstractController
{

    private $em ;
    private $parameterBag;
    function __construct(EntityManagerInterface $manager, ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
        $this->em =  $manager;
    }


    /**
     * @Rest\Get(path="/chapitre/{id}", name="show_chapitre")
     * @Rest\View()
     */
    public function getChapitre(Chapitre $chapitre){

        return $chapitre;

    }

    /**
     * @Rest\Post(path="/saveChapitre/", name="save_chapitre")
     * @Rest\View()
     */
    public  function saveChapitre(Request $request){

        /**
         * @var UploadedFile $pdf
         */
        $pdf = $request->files->get('pdf');
        $description = $request->request->get('description');
        $titre = $request->request->get('titre');
        $livre_id = $request->request->get('livre');

        $info_miniature = pathinfo($pdf->getClientOriginalName());

        $nom_pdf = $titre.'_'.strval(random_int(1, 100)).'.pdf';



        $chapitre = new Chapitre();
        /**
         * @var Livre $livre
         */
        $livre = $this->em->getRepository(Livre::class)->find($livre_id);
        $chapitre->setSonLivre($livre);
        if ( strtolower($info_miniature['extension']) != 'pdf'){
            return new Response(json_encode(["reponse"=>"mauvaise extension de fichier"]), 500);
        }

        try {
            $pdf->move($this->parameterBag->get('kernel.project_dir') .'/public/pdf', $nom_pdf);

        } catch (FileException $e) {
            dd($e);
            return  new Response(json_encode(["reponse"=>"ko"]), 500);
            // ... handle exception if something happens during file upload
        }
        $chapitre->setCreateAt(new \DateTime('now'));
        $chapitre->setDescription($description);
        $chapitre->setTitre($titre);
        $chapitre->setPdfPath('pdf/'.$nom_pdf);


        $this->em->persist($chapitre);
        $this->em->flush();


        return $chapitre;
    }

    /**
     * @Rest\Get(path="/chapitres/", name="show_chapitres")
     * @Rest\View()
     */
    public function getChapitres(Request $request){
        $pagination = new  Paginator();
        $chapitres = $this->getDoctrine()->getRepository(Chapitre::class)->findAll();

        $chapitres = $pagination->paginate($chapitres, $request->query->getInt('page', 1), 8);
        return $chapitres;

    }

    /**
     * @Rest\Get(path="/livre/chapitre/{id}", name="show_chapitres")
     * @Rest\View()
     */
    public function getChapitreFromLivre(Request $request, $id){

        /**
         * @var Livre $livre
         */
        $livre = $this->getDoctrine()->getRepository(Livre::class)->find($id);

        $chapitres = $livre->getChapitres();


        return $chapitres;

    }
    /**
     * @Rest\Delete(path="/delete_chapitre/{id}", name="delete_chapitre")
     */
    public function deleteChapitre(Chapitre $chapitre){

        try{
            unlink($this->parameterBag->get('kernel.project_dir') . '/public/'.$chapitre->getPdfPath());
            $this->em->remove($chapitre);
            $this->em->flush();
            return new Response(json_encode(["reponse"=>"ok"]), 200);
        } catch (\Exception $exception){

            return  new Response(json_encode(["reponse"=>"ko"]), 500);
        }
    }

}