<?php
/**
 * Created by PhpStorm.
 * User: Franklin M
 * Date: 7/4/2020
 * Time: 10:02 AM
 */

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Chapitre;
use App\Entity\Commentaire;
use App\Repository\CommentaireRepository;
use FOS\RestBundle\Controller\Annotations as Rest;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Rest\Route(path="/api")
 * Class CommentaireController
 * @package App\Controller
 */
class CommentaireController extends  AbstractController
{
    /**
     * @Rest\Get(path="/commentaire/{id}", name="show_commentaire", requirements={"id" = "\d+"})
     * @Rest\View()
     */
    public  function showCommentaire(CommentaireRepository $commentaireRepository, $id){
        $commentaire = $commentaireRepository->find($id);
        if (!$commentaire){
            $commentaire = "Ce commentaire n'existe pas";
        }
    return $commentaire;
    }

    /**
     * @Rest\Post(path="/commentaire/", name="commentaire_add" )
     * @Rest\View()
     * @ParamConverter("commentaire", converter="fos_rest.request_body")
     */
    public function saveCommentaire(Commentaire $commentaire){

        $em = $this->getDoctrine()->getManager();
        $em->persist($commentaire);
        $em->flush();

        return $commentaire;
    }


    /**
     * @Rest\Get(path="/commentaires/", name="show_commentaires")
     * @Rest\View()
     */
    public  function showCommentaires( Request $request){
        $pagination = new  Paginator();
        $commentaire = $this->getDoctrine()->getRepository(Commentaire::class)->findAll();

        $commentaire = $pagination->paginate($commentaire, $request->query->getInt('page', 1), 8);
        return $commentaire;
    }

    /**
     * @Rest\Get(path="/chapitre/commentaires/{id}", name="show_commentaire")
     * @Rest\View()
     */
    public function getCommentaireFromChapitre(Request $request, $id){

        /**
         * @var Chapitre $chapitre
         */
        $chapitre = $this->getDoctrine()->getRepository(Chapitre::class)->find($id);

        $commentaire = $chapitre->getCommentaires();


        return $commentaire;

    }

    /**
     * @Rest\Delete(path="/commentaire/{id}", name="delete_commentaire", requirements={"id" = "\d+"})
     * @Rest\View()
     */
    public  function deleteCommentaire(Commentaire $commentaire){

        $em = $this->getDoctrine()->getManager();
        $em->remove($commentaire);
        $em->flush();
        return ;
    }

}