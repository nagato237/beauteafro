<?php
/**
 * Created by PhpStorm.
 * User: Franklin M
 * Date: 7/22/2020
 * Time: 4:12 PM
 */

namespace App\Controller;
use App\Entity\Abonnement;
use App\Entity\Facture;
use FOS\RestBundle\Controller\Annotations as Rest;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Rest\Route(path="/api")
 * Class FactureController
 * @package App\Controller
 */

class FactureController extends AbstractController
{

    /**
     * @Rest\Get(path="/facture/{id}", name="show_facture")
     * @Rest\View()
     */
    public function getFacture(Facture $facture){

        return $facture;

    }

    /**
     * @Rest\Post(path="/savefacture/", name="save_facture")
     * @Rest\View()
     * @ParamConverter("facture", converter="fos_rest.request_body")
     */
    public  function saveFacture(Facture $facture){
        $em = $this->getDoctrine()->getManager();
        $em->persist($facture);
        $em->flush();

        return $facture;
    }

    /**
     * @Rest\Get(path="/factures/", name="show_factures")
     * @Rest\View()
     */
    public function getFactures(Request $request){
        $pagination = new  Paginator();
        $facture = $this->getDoctrine()->getRepository(Facture::class)->findAll();

        $facture = $pagination->paginate($facture, $request->query->getInt('page', 1), 8);
        return $facture;

    }


}