<?php
/**
 * Created by PhpStorm.
 * User: Franklin M
 * Date: 7/22/2020
 * Time: 4:12 PM
 */

namespace App\Controller;
use App\Entity\Categorie;
use App\Entity\Livre;

use App\Entity\Video;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class HomeController
 * @package App\Controller
 */
class HomeController extends AbstractController
{
   private $em ;
    private $parameterBag ;
    function __construct(EntityManagerInterface $manager, ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
        $this->em =  $manager;
    }

    /**
     * @Rest\Get(path="/home", name="home")
     * @Rest\View()
     */
    public function getlastHome(Request $request){

        $last_livres = $this->em->getRepository(Livre::class)->findBy([],['id'=> 'DESC'], 3);
        $last_videos= $this->em->getRepository(Video::class)->findBy([],['id'=> 'DESC'], 3);
        $home_data['livres'] = $last_livres;
        $home_data['videos'] = $last_videos;
        return $home_data;

    }



    /**
     * @Rest\Get(path="/homelivres", name="home_livres")
     * @Rest\View()
     */
    public function getlivres(Request $request){
        $pagination = new  Paginator();
        $livres = $this->getDoctrine()->getRepository(Livre::class)->findAll();

        $livres = $pagination->paginate($livres, $request->query->getInt('page', 1), 8);
        return $livres;

    }

    /**
     * @Rest\Get(path="/homevideos/", name="home_videos")
     * @Rest\View()
     */
    public function getvideos(Request $request){
        $pagination = new  Paginator();
        $video = $this->getDoctrine()->getRepository(Video::class)->findAll();

        $video = $pagination->paginate($video, $request->query->getInt('page', 1), 8);
        return $video;

    }

    /**
     * @Rest\Delete(path="/delete_livre/{id}", name="delete_livre")
     */
    public function deleteChapitre(Livre $livre){

        try{
            unlink($this->parameterBag->get('kernel.project_dir') . '/public/'.$livre->getPathMiniature());
            $this->em->remove($livre);
            $this->em->flush();
            return new Response(json_encode(["reponse"=>"ok"]), 200);
        } catch (\Exception $exception){

            return  new Response(json_encode(["reponse"=>"ko"]), 500);
        }
    }

}