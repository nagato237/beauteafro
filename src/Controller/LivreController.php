<?php
/**
 * Created by PhpStorm.
 * User: Franklin M
 * Date: 7/22/2020
 * Time: 4:12 PM
 */

namespace App\Controller;
use App\Entity\Categorie;
use App\Entity\Livre;

use App\Services\PaiementService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route(path="/api")
 * Class LivreController
 * @package App\Controller
 */
class LivreController extends AbstractController
{
   private $em ;
    private $parameterBag ;
    function __construct(EntityManagerInterface $manager, ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
        $this->em =  $manager;
    }

    /**
     * @Rest\Get(path="/livre/{id}", name="show_livre")
     * @Rest\View()
     */
    public function getlivre(Livre $livre, PaiementService $paiementService){

        if (!$paiementService->verification_abonnement($this->getUser())){
            return  new Response(json_encode(["reponse"=>"abo_ko"]), 403);
        }

        return $livre;

    }

    /**
     * @Rest\Post(path="/savelivre/", name="save_livre")
     * @Rest\View()
     */
    public  function savelivre(Request $request, LoggerInterface $logger){

        /**
         * @var UploadedFile $miniature
         */
        $miniature = $request->files->get('miniature');
        $description = $request->request->get('description');
        $titre = $request->request->get('titre');
        $categorie_id = $request->request->get('categorie');

        $logger->error('UNE ERREUR SURVENUE : '.$categorie_id);
        $info_miniature = pathinfo($miniature->getClientOriginalName());

        $nom_miniature = $titre.'_'.strval(random_int(1, 100)).'.jpg';

        if ( strtolower($info_miniature['extension']) != 'jpg'){
            return new Response(json_encode(["reponse"=>"mauvaise extension de fichier"]), 500);
        }

        try {
            $miniature->move($this->parameterBag->get('kernel.project_dir') .'/public/miniatures', $nom_miniature);

        } catch (FileException $e) {

            return  new Response(json_encode(["reponse"=>"ko"]), 500);
            // ... handle exception if something happens during file upload
        }

        $livre = new Livre();
        /**
         * @var Categorie $categrorie
         */
        $categrorie = $this->em->getRepository(Categorie::class)->find($categorie_id);
        $livre->setCategorie($categrorie);

        $livre->setCreateAt(new \DateTime('now'));
        $livre->setDescription($description);
        $livre->setTitre($titre);
        $livre->setPathMiniature('miniatures/'.$nom_miniature);


        $this->em->persist($livre);
        $this->em->flush();


        return $livre;
    }

    /**
     * @Rest\Get(path="/livres/", name="show_livres")
     * @Rest\View()
     */
    public function getlivres(Request $request){
        $pagination = new  Paginator();
        $livres = $this->getDoctrine()->getRepository(Livre::class)->findAll();

        $livres = $pagination->paginate($livres, $request->query->getInt('page', 1), 8);
        return $livres;

    }

    /**
     * @Rest\Delete(path="/delete_livre/{id}", name="delete_livre")
     */
    public function deleteChapitre(Livre $livre){

        try{
            unlink($this->parameterBag->get('kernel.project_dir') . '/public/'.$livre->getPathMiniature());
            $this->em->remove($livre);
            $this->em->flush();
            return new Response(json_encode(["reponse"=>"ok"]), 200);
        } catch (\Exception $exception){

            return  new Response(json_encode(["reponse"=>"ko"]), 500);
        }
    }

}