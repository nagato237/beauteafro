<?php
/**
 * Created by PhpStorm.
 * User: Franklin M
 * Date: 7/4/2020
 * Time: 10:02 AM
 */

namespace App\Controller;

use App\Entity\Chapitre;
use App\Entity\Commentaire;
use App\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**

 * Class MessageController
 * @package App\Controller
 */
class MessageController extends  AbstractController
{
    /**
     * @Rest\Get(path="/commentaire/{id}", name="show_message", requirements={"id" = "\d+"})
     * @Rest\View()
     */
    public  function showMessages(Chapitre $chapitre){
        $commentaires = $this->getDoctrine()->getRepository(Commentaire::class)->findBy(["chapitre"=>$chapitre]);
    return $commentaires;
    }

    /**
     * @Rest\Post(path="/savecommentaire/", name="commentaire_add" )
     * @Rest\View()
     */
    public function saveCommentaire( Request $request){
        $commentaire = new Commentaire; 
        $em = $this->getDoctrine()->getManager();
        $commentaire->setCreateAt(new \DateTime());
        $commentaire->setContenu($request->request->get('contenu'));
        $commentaire->setUser($this->getDoctrine()->getRepository(User::class)->find(17));
        $commentaire->setChapitre($this->getDoctrine()->getRepository(Chapitre::class)->find($request->request->get('chapitre')));
        $em->persist($commentaire);
        $em->flush();

        return $commentaire;
    }


    /**
     * @Rest\Get(path="/commentaires/", name="show_commentaire")
     * @Rest\View()
     */
    public  function showAllCommentaires( Request $request){
        $pagination = new  Paginator();
        $commentaires = $this->getDoctrine()->getRepository(Commentaire::class)->findAll();

        $commentaires = $pagination->paginate($commentaires, $request->query->getInt('page', 1), 50);
        return $commentaires;
    }

}