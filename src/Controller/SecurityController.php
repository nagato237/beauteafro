<?php
/**
 * Created by PhpStorm.
 * User: Franklin M
 * Date: 7/22/2020
 * Time: 4:12 PM
 */

namespace App\Controller;
use App\Entity\Abonnement;
use App\Entity\Facture;
use App\Entity\User;
use App\Evenements\PasswordEncoderSubscriber;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Knp\Component\Pager\Paginator;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTDecodedEvent;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 *
 * Class SecurityController
 * @package App\Controller
 */
class SecurityController  extends AbstractController
{

    private $em;

    public  function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
 * @Rest\Get(path="/user/{id}", name="show_user")
 * @Rest\View()
 */
    public function getOne_User(User $user){

        return $user;

    }

    /**
     * @Rest\Post(name="api_login", path="/api/login_check")
     */
    public function api_login(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $userPasswordEncoder) {

        /**
         * @var User $user
         */
        $user = $this->getUser();

        if (!$user){
            return new JsonResponse("l'utilisareur nexiste pas");
        }


        return  (['email' => $user->getUsername(), 'roles' => $user->getRoles()]);
    }



}