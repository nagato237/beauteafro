<?php
/**
 * Created by PhpStorm.
 * User: Franklin M
 * Date: 7/22/2020
 * Time: 4:12 PM
 */

namespace App\Controller;
use App\Entity\Abonnement;
use App\Entity\Facture;
use App\Entity\User;
use App\Evenements\PasswordEncoderSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Rest\Route(path="/api")
 * Class UserController
 * @package App\Controller
 */
class UserController extends AbstractController
{
    private $encoder;
    private $em;

    public  function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $em)
    {
        $this->encoder = $encoder;
        $this->em = $em;

    }

    /**
 * @Rest\Get(path="/user/{id}", name="show_user")
 * @Rest\View()
 */
    public function getOne_User(User $user){

        return $user;

    }

    /**
     * @Rest\Post(path="/save_user/", name="save_user")
     * @Rest\View()
     * @ParamConverter("user", converter="fos_rest.request_body")
     */
    public  function saveUser(User $user){
     try{
         //hashage du mot du mot de passe
         $hash=  $this->encoder->encodePassword($user, $user->getPassword());
         $user->setPassword($hash);
         $user->setIsActive(1);

         $this->em->persist($user);
         $this->em->flush();

         return $user;
     }catch (Exception $e){
         return new Response(json_encode(["reponse"=>"ko"]), 500);
     }
    }

    /**
     * @Rest\Get(path="/users/", name="show_users")
     * @Rest\View()
     */
    public function getUsers(Request $request){

        $pagination = new  Paginator();
        $user = $this->getDoctrine()->getRepository(User::class)->findAll();

        $user = $pagination->paginate($user, $request->query->getInt('page', 1), 8);
        return $user;

    }

    /**
     * @Rest\Delete(path="/delete_user/{id}", name="delete_user")
     */
    public function deleteUser(User $user){

        try{
            $this->em->remove($user);
            $this->em->flush();
            return new Response(json_encode(["reponse"=>"ok"]), 200);
        } catch (\Exception $exception){

            return  new Response(json_encode(["reponse"=>"ko"]), 500);
        }
    }

}