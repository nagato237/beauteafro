<?php
/**
 * Created by PhpStorm.
 * User: Franklin M
 * Date: 7/22/2020
 * Time: 4:12 PM
 */

namespace App\Controller;
use App\Entity\Categorie;
use App\Entity\Livre;

use App\Entity\Video;
use App\Services\PaiementService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Rest\Route(path="/api")
 * Class VideoController
 * @package App\Controller
 */
class VideoController extends AbstractController
{
   private $em ;
    private $parameterBag ;
    function __construct(EntityManagerInterface $manager, ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
        $this->em =  $manager;
    }

    /**
     * @Rest\Get(path="/video/{id}", name="getvideo")
     * @Rest\View()
     */
    public function getvideo(Video $video, PaiementService $paiementService){

        if (!$paiementService->verification_abonnement($this->getUser())){
            return  new Response(json_encode(["reponse"=>"abo_ko"]), 403);
        }

        return $video;

    }

    /**
     * @Rest\Post(path="/savevideo/", name="save_video")
     * @Rest\View()
     */
    public  function savelivre(Request $request){

        dump('ok rentré');
        /**
         * @var UploadedFile $miniature
         */
        $miniature = $request->files->get('miniature');
        $description = $request->request->get('description');
        $titre = $request->request->get('titre');
        $path = $request->request->get('path');

        $info_miniature = pathinfo($miniature->getClientOriginalName());

        $nom_miniature = $titre.'_'.strval(random_int(1, 100)).'.jpg';

        if ( strtolower($info_miniature['extension']) != 'jpg'){
            return new Response(json_encode(["reponse"=>"mauvaise extension de fichier"]), 500);
        }

        try {
            $miniature->move($this->parameterBag->get('kernel.project_dir') .'/public/miniatures', $nom_miniature);

        } catch (FileException $e) {
            dd($e);
            return  new Response(json_encode(["reponse"=>"ko"]), 500);
            // ... handle exception if something happens during file upload
        }

        $video = new Video();

        $video->setCreateAt(new \DateTime('now'));
        $video->setDescription($description);
        $video->setTitre($titre);
        $video->setTitre($path);
        $video->setMiniature('miniatures/'.$nom_miniature);


        $this->em->persist($video);
        $this->em->flush();


        return $video;
    }

    /**
     * @Rest\Get(path="/videos/", name="show_videos")
     * @Rest\View()
     */
    public function getvideos(Request $request){
        $pagination = new  Paginator();
        $video = $this->getDoctrine()->getRepository(Video::class)->findAll();

        $video = $pagination->paginate($video, $request->query->getInt('page', 1), 8);
        return $video;

    }

    /**
     * @Rest\Delete(path="/delete_video/{id}", name="delete_video")
     */
    public function deletevideos(Video $video){

        try{
            unlink($this->parameterBag->get('kernel.project_dir') . '/public/'.$video->getMiniature());
            $this->em->remove($video);
            $this->em->flush();
            return new Response(json_encode(["reponse"=>"ok"]), 200);
        } catch (\Exception $exception){

            return  new Response(json_encode(["reponse"=>"ko"]), 500);
        }
    }

}