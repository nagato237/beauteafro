<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategorieRepository")
 * @ApiResource()
 */
class Categorie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $intitule;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Video", mappedBy="categorieVideo")
     */
    private $categorieVideo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Livre", mappedBy="categorie")
     */
    private $livres;

    public function __construct()
    {
        $this->categorieVideo = new ArrayCollection();
        $this->livres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Video[]
     */
    public function getCategorieVideo(): Collection
    {
        return $this->categorieVideo;
    }

    public function addCategorieVideo(Video $categorieVideo): self
    {
        if (!$this->categorieVideo->contains($categorieVideo)) {
            $this->categorieVideo[] = $categorieVideo;
            $categorieVideo->addCategorieVideo($this);
        }

        return $this;
    }

    public function removeCategorieVideo(Video $categorieVideo): self
    {
        if ($this->categorieVideo->contains($categorieVideo)) {
            $this->categorieVideo->removeElement($categorieVideo);
            $categorieVideo->removeCategorieVideo($this);
        }

        return $this;
    }

    /**
     * @return Collection|Livre[]
     */
    public function getLivres(): Collection
    {
        return $this->livres;
    }

    public function addLivre(Livre $livre): self
    {
        if (!$this->livres->contains($livre)) {
            $this->livres[] = $livre;
            $livre->setCategorie($this);
        }

        return $this;
    }

    public function removeLivre(Livre $livre): self
    {
        if ($this->livres->contains($livre)) {
            $this->livres->removeElement($livre);
            // set the owning side to null (unless already changed)
            if ($livre->getCategorie() === $this) {
                $livre->setCategorie(null);
            }
        }

        return $this;
    }
}
