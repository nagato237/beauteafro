<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\VideoRepository")
 */
class Video
{


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $miniature;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;



    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Categorie", inversedBy="categorieVideo")
     */
    private $categorieVideo;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    public function __construct()
    {
        $this->categorieVideo = new ArrayCollection();
        $this->createAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }



    /**
     * @return Collection|Categorie[]
     */
    public function getCategorieVideo(): Collection
    {
        return $this->categorieVideo;
    }

    public function addCategorieVideo(Categorie $categorieVideo): self
    {
        if (!$this->categorieVideo->contains($categorieVideo)) {
            $this->categorieVideo[] = $categorieVideo;
        }

        return $this;
    }

    public function removeCategorieVideo(Categorie $categorieVideo): self
    {
        if ($this->categorieVideo->contains($categorieVideo)) {
            $this->categorieVideo->removeElement($categorieVideo);
        }

        return $this;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMiniature()
    {
        return $this->miniature;
    }

    /**
     * @param mixed $miniature
     */
    public function setMiniature($miniature)
    {
        $this->miniature = $miniature;
    }


}
