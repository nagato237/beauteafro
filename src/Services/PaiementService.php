<?php
/**
 * Created by PhpStorm.
 * User: Franklin M
 * Date: 6/27/2021
 * Time: 4:59 PM
 */

namespace App\Services;


use App\Entity\Facture;
use App\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;

class PaiementService
{
    private $em;
    public  function __construct(ManagerRegistry $manager)
    {
        $this->em = $manager;
    }

    /**
     * fonction de verification d'abonnement d'un user (booleen)
     */
    public function verification_abonnement($user){
        $etat = false;
        if (!$user){
            /**
             * @var User $user
             */
            $user = $this->em->getRepository(User::class)->find($user->getId());
        }
        /**
         * @var Facture $last_facture
         */
        $last_facture = $this->em->getRepository(Facture::class)->findOneBy(['user'=> $user->getId()],['id'=>'DESC'],1,0);
        if ($last_facture){
            $date = new \DateTime('now');
            $interval = date_diff( $date, $last_facture->getFinishedAt());
           $etat = ($interval->format('%R%a') < 0)? false :  true;
        }


        return $etat;
    }
}